import axios from 'axios'

export default class Api {

  constructor (apiServer, apiPrefix, clientId, clientSecret) {
    this.apiServer = apiServer;
    this.apiPrefix = apiPrefix;
    this.clientId = clientId;
    this.clientSecret = clientSecret;
  }

  auth(login, pass, retFunc, retFuncFail) {
    this.request(
      'post',
      'login',
      false,
      {
        email: login,
        password: pass
      },
      retFunc,
      retFuncFail
    )
  }

  register(email, pass, userData, retFunc, retFuncFail) {
    userData['email'] = email;
    userData['password'] = pass;
    userData['password_confirmation'] = pass;

    this.request(
      'post',
      'register',
      false,
      userData,
      retFunc,
      retFuncFail
    )
  }

  createLanding(apiKeyAuth, landingData, retFunc, retFuncFail) {
    this.request(
      'postFile',
      'manager/landing',
      apiKeyAuth,
      landingData,
      retFunc,
      retFuncFail
    )
  }

  listLanding(apiKeyAuth, retFunc, retFuncFail) {
    this.request(
      'get',
      'manager/landing',
      apiKeyAuth,
      false,
      retFunc,
      retFuncFail
    )
  }

  getLanding(apiKeyAuth, landingId, retFunc, retFuncFail) {
    this.request(
      'get',
      'manager/landing/'+landingId,
      apiKeyAuth,
      false,
      retFunc,
      retFuncFail
    )
  }

  updateLanding(apiKeyAuth, id, modelData, retFunc, retFuncFail) {
    this.request(
      'postFile',
      'manager/landing/' + id,
      apiKeyAuth,
      modelData,
      retFunc,
      retFuncFail
    )
  }

  getClientLanding(landingId, retFunc, retFuncFail) {
    this.request(
      'get',
      'landing/'+landingId,
      false,
      false,
      retFunc,
      retFuncFail
    )
  }

  getClientLandLinks(landingId, retFunc, retFuncFail) {
    this.request(
      'get',
      'landlink',
      false,
      {
        landing_id: landingId
      },
      retFunc,
      retFuncFail
    )
  }

  listClientLandBanners(landingId, retFunc, retFuncFail) {
    this.request(
      'get',
      'landbanner',
      false,
      {
        landing_id: landingId
      },
      retFunc,
      retFuncFail
    )
  }

  createLandLink(apiKeyAuth, landId, linkData, retFunc, retFuncFail) {
    linkData['land_id'] = landId;
    this.request(
      'post',
      'manager/landlink',
      apiKeyAuth,
      linkData,
      retFunc,
      retFuncFail
    )
  }

  updateLandLink(apiKeyAuth, linkId, linkData, retFunc, retFuncFail) {
    this.request(
      'put',
      'manager/landlink/' + linkId,
      apiKeyAuth,
      linkData,
      retFunc,
      retFuncFail
    )
  }

  listLandLink(apiKeyAuth, filterData, retFunc, retFuncFail) {
    this.request(
      'get',
      'manager/landlink',
      apiKeyAuth,
      filterData,
      retFunc,
      retFuncFail
    )
  }

  createLandBanner(apiKeyAuth, landId, bannerData, retFunc, retFuncFail) {
    bannerData['landing_id'] = landId
    this.request(
      'postFile',
      'manager/landbanner',
      apiKeyAuth,
      bannerData,
      retFunc,
      retFuncFail
    )
  }


  listLandBanner(apiKeyAuth, filterData, retFunc, retFuncFail) {
    this.request(
      'get',
      'manager/landbanner',
      apiKeyAuth,
      filterData,
      retFunc,
      retFuncFail
    )
  }

  createProduct(apiKeyAuth, landId, modelData, retFunc, retFuncFail) {
    modelData['landing_id'] = landId
    this.request(
      'post',
      'manager/product',
      apiKeyAuth,
      modelData,
      retFunc,
      retFuncFail
    )
  }

  listProducts(apiKeyAuth, filterData, retFunc, retFuncFail) {
    this.request(
      'get',
      'manager/product',
      apiKeyAuth,
      filterData,
      retFunc,
      retFuncFail
    )
  }

  getProduct(apiKeyAuth, id, retFunc, retFuncFail) {
    this.request(
      'get',
      'manager/product/'+id,
      apiKeyAuth,
      false,
      retFunc,
      retFuncFail
    )
  }

  updateProduct(apiKeyAuth, id, modelData, retFunc, retFuncFail) {
    this.request(
      'put',
      'manager/product/' + id,
      apiKeyAuth,
      modelData,
      retFunc,
      retFuncFail
    )
  }

  deleteProduct(apiKeyAuth, id, retFunc, retFuncFail) {
    this.request(
      'delete',
      'manager/product/'+id,
      apiKeyAuth,
      false,
      retFunc,
      retFuncFail
    )
  }

  createImageProduct(apiKeyAuth, modelData, retFunc, retFuncFail) {
    this.request(
      'postFile',
      'manager/image-product',
      apiKeyAuth,
      modelData,
      retFunc,
      retFuncFail
    )
  }

  attachImageProduct(apiKeyAuth, imageId, productId, retFunc, retFuncFail) {
    this.request(
      'post',
      'manager/product/' + productId + '/attach-image',
      apiKeyAuth,
      {
        image_product_id: imageId
      },
      retFunc,
      retFuncFail
    )
  }

  confirmSms(phone, code, retFunc, retFuncFail) {
    this.request(
      'post',
      'checksms',
      false,
      {
        phone: phone,
        code: code
      },
      retFunc,
      retFuncFail
    )
  }

  checkEmail(email, retFunc, retFuncFail) {
    this.request(
      'post',
      'checkemail',
      false,
      {
        email: email
      },
      retFunc,
      retFuncFail
    )
  }

  checkUrlcode(code, retFunc, retFuncFail) {
    this.request(
      'post',
      'checkurlcode',
      false,
      {
        code: code
      },
      retFunc,
      retFuncFail
    )
  }

  getAccount(apiKeyAuth, retFunc, retFuncFail) {
    this.request(
      'get',
      'manager/user',
      apiKeyAuth,
      false,
      retFunc,
      retFuncFail
    )
  }

  updateAccount(apiKeyAuth, modelData, retFunc, retFuncFail) {
    this.request(
      'put',
      'manager/user',
      apiKeyAuth,
      modelData,
      retFunc,
      retFuncFail
    )
  }

  listClientProduct(apiKeyAuth, landingId, retFunc, retFuncFail) {
    this.request(
      'get',
      '/product',
      apiKeyAuth,
      {
        landing_id: landingId
      },
      retFunc,
      retFuncFail
    )
  }

  makeDefaultLanding(apiKeyAuth, retFunc, retFuncFail) {
    this.request(
      'post',
      'manager/landing/make-default',
      apiKeyAuth,
      false,
      retFunc,
      retFuncFail
    )
  }

  getUrlInstagramOAuth(retFunc, retFuncFail) {
    this.request(
      'get',
      'auth/instagram',
      false,
      false,
      retFunc,
      retFuncFail
    )
  }

  getPaymentInitUrl(apiKeyAuth, retFunc, retFuncFail) {
    this.request(
      'post',
      'manager/payment/init',
      apiKeyAuth,
      false,
      retFunc,
      retFuncFail
    )
  }

  forgotPassword(apiKeyAuth, email, retFunc, retFuncFail) {
    this.request(
      'post',
      'forgot-password',
      apiKeyAuth,
      {
        email: email
      },
      retFunc,
      retFuncFail
    )
  }

  resetPassword(apiKeyAuth, email, token, newPassword, retFunc, retFuncFail) {
    this.request(
      'post',
      'reset-password',
      apiKeyAuth,
      {
        email: email,
        token: token,
        password: newPassword,
        password_confirmation: newPassword
      },
      retFunc,
      retFuncFail
    )
  }

  changePassword(apiKeyAuth, newPassword, retFunc, retFuncFail) {
    this.request(
      'post',
      'change-password',
      apiKeyAuth,
      {
        password: newPassword,
      },
      retFunc,
      retFuncFail
    )
  }

  request(type, path, apiKeyAuth, data, promiseFuncSuccess, promiseFuncFail) {
    var config = {};
    if(apiKeyAuth) {
      config['headers'] = {
        'Authorization': "Bearer " + apiKeyAuth
      }
    }

    var url = this.apiServer + this.apiPrefix + path;
    if (type === 'post') {
      axios.post(
        url,
        data,
        config
      ).then(
        function(response) {
          if(promiseFuncSuccess) {
            promiseFuncSuccess(response.status, response.data)
          }
        }
      ).catch(
        function(exceptData) {
          if(exceptData.response) {
            if(promiseFuncFail) {
              promiseFuncFail(exceptData.response.status, exceptData.response.data)
            } else {
              console.log(exceptData.response.data)
            }
          }
        }
      )
    } else if(type === 'get') {
      if(data) {
        config['params'] = data
      }

      axios.get(
        url,
        config
      ).then(
        function(response) {
          if(promiseFuncSuccess) {
            promiseFuncSuccess(response.status, response.data)
          }
        }
      ).catch(
        function(exceptData) {
          if(exceptData.response) {
            if(promiseFuncFail) {
              promiseFuncFail(exceptData.response.status, exceptData.response.data)
            } else {
              console.log(exceptData.response.data)
            }
          }
        }
      )
    } else if (type === 'put') {
      axios.put(
        url,
        data,
        config
      ).then(
        function(response) {
          if(promiseFuncSuccess) {
            promiseFuncSuccess(response.status, response.data)
          }
        }
      ).catch(
        function(exceptData) {
          if(exceptData.response) {
            if(promiseFuncFail) {
              promiseFuncFail(exceptData.response.status, exceptData.response.data)
            } else {
              console.log(exceptData.response.data)
            }
          } else {
            throw new Error(exceptData.message)
          }
        }
      )
    } else if (type === 'postFile') {
      var formData = new FormData();
      for(var i in data) {
        formData.append(i, data[i]);
      }
      axios.post(
        url,
        formData,
        config
      ).then(
        function(response) {
          if(promiseFuncSuccess) {
            promiseFuncSuccess(response.status, response.data)
          }
        }
      ).catch(
        function(exceptData) {
          if(exceptData.response) {
            if(promiseFuncFail) {
              promiseFuncFail(exceptData.response.status, exceptData.response.data)
            } else {
              console.log(exceptData.response.data)
            }
          }
        }
      )
    } else if (type === 'delete') {
      if(data) {
        config['params'] = data
      }
      axios.delete(
        url,
        config
      ).then(
        function(response) {
          if(promiseFuncSuccess) {
            promiseFuncSuccess(response.status, response.data)
          }
        }
      ).catch(
        function(exceptData) {
          if(exceptData.response) {
            if(promiseFuncFail) {
              promiseFuncFail(exceptData.response.status, exceptData.response.data)
            } else {
              console.log(exceptData.response.data)
            }
          } else {
            throw new Error(exceptData.message)
          }
        }
      )
    }
  }
}
