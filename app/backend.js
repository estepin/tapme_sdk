import Api from "./api";

export default class Backend {
  constructor(apiServer, apiPrefix, clientId, clientSecret) {
    this.apiServer = apiServer;
    this.api = new Api(apiServer, apiPrefix, clientId, clientSecret)
  }

  auth(login, pass, funcSuccess, funcFail) {
    this.api.auth(
      login,
      pass,
      function(status, data) {
        if(data['api_token']) {
          this.setApiKeyAuth(data['api_token'])
        }
        funcSuccess(data)
      }.bind(this),
      function(status, data) {
        funcFail(data)
      }
    )
  }

  isAuth(funcSuccess, funcFail) {
    if(!this.getApiKeyAuth()) {
      if(funcFail) {
        funcFail();
      }
    } else {
      this.getAccount(
        () => {
          if(funcSuccess) {
            funcSuccess();
          }
        },
        () => {
          if(funcFail) {
            funcFail();
          }
        }
      )
    }
  }

  register(login, pass, userData, funcSuccess, funcFail) {
    this.api.register(
      login,
      pass,
      userData,
      function(status, data) {
        if(data['api_token']) {
          this.setApiKeyAuth(data['api_token'])
        }
        funcSuccess(data)
      }.bind(this),
      function(status, data) {
        funcFail(data)
      }
    )
  }

  getApiKeyAuth() {
    var apiKey = localStorage.getItem('api_token');

    if(apiKey)
      return apiKey;
  }

  setApiKeyAuth(value) {
    localStorage.setItem('api_token', value)
  }

  clearApiKeyAuth() {
    localStorage.removeItem('api_token');
  }

  confirmSms(phone, code, funcSuccess, funcFail) {
    this.api.confirmSms(
      phone,
      code,
      function(status, data) {
        funcSuccess(data)
      },
      function(status, data) {
        funcFail(data)
      }
    )
  }

  checkEmail(email, funcSuccess, funcFail) {
    this.api.checkEmail(
      email,
      function(status, data) {
        funcSuccess(data)
      },
      function(status, data) {
        funcFail(data)
      }
    )
  }

  createLanding(landingData, funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.createLanding(
      apiKey,
      landingData,
      function(status, data) {
        funcSuccess(data)
      },
      function(status, data) {
        funcFail(data)
      }
    )
  }

  createProduct(modelData, landId, funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.createProduct(
      apiKey,
      landId,
      modelData,
      function(status, data) {
        funcSuccess(data)
      },
      function(status, data) {
        funcFail(data)
      }
    )
  }

  getProduct(id, funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.getProduct(
      apiKey,
      id,
      function(status, data) {
        funcSuccess(data)
      },
      function(status, data) {
        funcFail(data)
      }
    )
  }

  listProducts(filterData, funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.listProducts(
      apiKey,
      filterData,
      function(status, data) {
        for(var i in data) {
          if(data[i]['images'] && data[i]['images'].length > 0) {
            for(var a in data[i]['images']) {
              data[i]['images'][a]['img_src'] = this.imageUrlTo('imageProduct', data[i]['images'][a]['img_src']);
            }
          }
        }
        funcSuccess(data)
      }.bind(this),
      function(status, data) {
        funcFail(data)
      }
    )
  }

  updateProduct(id, modelData, funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();

    this.api.updateProduct(
      apiKey,
      id,
      modelData,
      function(status, data) {
        funcSuccess(data)
      }.bind(this),
      function(status, data) {
        funcFail(data)
      }
    )
  }

  deleteProduct(id, funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();

    this.api.deleteProduct(
      apiKey,
      id,
      function(status, data) {
        funcSuccess(data)
      }.bind(this),
      function(status, data) {
        funcFail(data)
      }
    )
  }

  createImageProduct(modelData, funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.createImageProduct(
      apiKey,
      modelData,
      function(status, data) {
        funcSuccess(data)
      },
      function(status, data) {
        funcFail(data)
      }
    )
  }

  attachImageProduct(imageId, productId, funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.attachImageProduct(
      apiKey,
      imageId,
      productId,
      function(status, data) {
        funcSuccess(data)
      },
      function(status, data) {
        funcFail(data)
      }
    )
  }

  listLanding(funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.listLanding(
      apiKey,
      function(status, data) {
        funcSuccess(data)
      },
      function(status, data) {
        funcFail(data)
      }
    )
  }

  getLanding(landingId, funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.getLanding(
      apiKey,
      landingId,
      function(status, data) {
        funcSuccess(data)
      }.bind(this),
      function(status, data) {
        funcFail(data)
      }
    )
  }

  getClientLanding(idOrCode, funcSuccess, funcFail) {
    this.api.getClientLanding(
      idOrCode,
      function(status, data) {
        funcSuccess(data)
      }.bind(this),
      function(status, data) {
        if(funcFail)
          funcFail(data)
      }
    )
  }

  imageUrlTo(type, image)
  {
    if(type == 'logo') {
      return this.apiServer + '/logo/' + image;
    } else if(type == 'banner') {
      return this.apiServer + '/banners/' + image;
    } else if(type == 'imageProduct') {
      return this.apiServer + '/storage/' + image;
    } else if(type == 'avatar') {
      return this.apiServer + '/storage/' + image;
    } else if(type == 'background') {
      return this.apiServer + '/storage/' + image;
    }
  }

  getClientLinks(landingId, funcSuccess, funcFail) {
    this.api.getClientLandLinks(
      landingId,
      function(status, data) {
        funcSuccess(data)
      },
      function(status, data) {
        funcFail(data)
      }
    )
  }

  listClientLandBanners(landingId, funcSuccess, funcFail) {
    this.api.listClientLandBanners(
      landingId,
      function(status, data) {
        for(var i in data) {
          data[i]['imagesrc'] = this.imageUrlTo('banner', data[i]['image_src']);
        }
        funcSuccess(data)
      }.bind(this),
      function(status, data) {
        funcFail(data)
      }
    )
  }

  createLandLink(linkData, landId, funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.createLandLink(
      apiKey,
      landId,
      linkData,
      function(status, data) {
        funcSuccess(data)
      },
      function(status, data) {
        if(funcFail)
          funcFail(data)
      }
    )
  }

  listLandLinks(filterData, funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.listLandLink(
      apiKey,
      filterData,
      function(status, data) {
        funcSuccess(data)
      }.bind(this),
      function(status, data) {
        funcFail(data)
      }
    )
  }

  updateLandLink(id, modelData, funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.updateLandLink(
      apiKey,
      id,
      modelData,
      function(status, data) {
        funcSuccess(data)
      }.bind(this),
      function(status, data) {
        funcFail(data)
      }
    )
  }

  updateLanding(id, modelData, funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.updateLanding(
      apiKey,
      id,
      modelData,
      function(status, data) {
        funcSuccess(data)
      }.bind(this),
      function(status, data) {
        funcFail(data)
      }
    )
  }

  createLandBanner(bannerData, landId, funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.createLandBanner(
      apiKey,
      landId,
      bannerData,
      function(status, data) {
        funcSuccess(data)
      },
      function(status, data) {
        funcFail(data)
      }
    )
  }

  getAccount(funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.getAccount(
      apiKey,
      function(status, data) {
        funcSuccess(data)
      },
      function(status, data) {
        funcFail(data)
      }
    )
  }

  updateAccount(modelData, funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.updateAccount(
      apiKey,
      modelData,
      function(status, data) {
        funcSuccess(data)
      },
      function(status, data) {
        funcFail(data)
      }
    )
  }

  checkUrlcode(code, funcSuccess, funcFail) {
    this.api.checkUrlcode(
      code,
      function(status, data) {
        funcSuccess(data)
      },
      function(status, data) {
        funcFail(data)
      }
    )
  }

  listClientProducts(landingId, funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.listClientProduct(
      apiKey,
      landingId,
      function(status, data) {
        for(var i in data) {
          if(data[i]['images'] && data[i]['images'].length > 0) {
            for(var a in data[i]['images']) {
              data[i]['images'][a]['img_src'] = this.imageUrlTo('imageProduct', data[i]['images'][a]['img_src']);
            }
          }
        }
        funcSuccess(data);
      }.bind(this),
      function(status, data) {
        funcFail(data)
      }
    )
  }

  makeDefaultLanding(funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.makeDefaultLanding(
      apiKey,
      function(status, data) {
        funcSuccess(data)
      },
      function(status, data) {
        funcFail(data)
      }
    )
  }

  logout(funcSuccess) {
    this.clearApiKeyAuth();
    if(funcSuccess)
      funcSuccess();
  }

  instagramAuthLink(funcSuccess, funcFail) {
    this.api.getUrlInstagramOAuth(
      function(status, data) {
        funcSuccess(data)
      },
      function(status, data) {
        funcFail(data)
      }
    )
  }

  paymentInitUrl(funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.getPaymentInitUrl(
      apiKey,
      function(status, data) {
        funcSuccess(data)
      },
      function(status, data) {
        funcFail(data)
      }
    )
  }

  forgotPassword(email, funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.forgotPassword(
      apiKey,
      email,
      function(status, data) {
        if(funcSuccess)
          funcSuccess(data)
      },
      function(status, data) {
        if(funcFail)
          funcFail(data)
      }
    )
  }

  resetPassword(email, token, newPassword, funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.resetPassword(
      apiKey,
      email,
      token,
      newPassword,
      function(status, data) {
        if(funcSuccess)
          funcSuccess(data)
      },
      function(status, data) {
        if(funcFail)
          funcFail(data)
      }
    )
  }

  changePassword(newPassword, funcSuccess, funcFail) {
    var apiKey = this.getApiKeyAuth();
    this.api.changePassword(
      apiKey,
      newPassword,
      function(status, data) {
        if(funcSuccess)
          funcSuccess(data)
      },
      function(status, data) {
        if(funcFail)
          funcFail(data)
      }
    )
  }
}
